using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public SphereController sphere;

    public TextMeshProUGUI aVelocityText;
    public TextMeshProUGUI rVelocityText;
    public TextMeshProUGUI distanceText;

    public TextMeshProUGUI angularControlText;
    public TextMeshProUGUI radialControlText;
    public TextMeshProUGUI accelerationControlText;

    public Slider angularSlider;
    public Slider radialSlider;
    public Slider accelerationSlider;

    public GameObject distancePanel;
    void Update()
    {
        UpdateText();
    }

    void UpdateText()
    {
        aVelocityText.text = "Angular velocity: " + Math.Round(sphere.AngleSpeed, 2);
        rVelocityText.text = "Radial velocity: " + Math.Round(sphere.RadiusSpeed, 2);

        distanceText.text = Math.Round(sphere.DistanceTraveled, 2).ToString();

        angularControlText.text = "Max angular velocity: " + angularSlider.value;
        radialControlText.text = "Max radial velocity: " + radialSlider.value;
        accelerationControlText.text = "Acceleration: " + accelerationSlider.value;
    }

    public void UpdateStartingValues()
    {
        sphere.maxAngleSpeed = angularSlider.value;
        sphere.maxRadiusSpeed = radialSlider.value;
        sphere.acceleration = accelerationSlider.value;
    }
}
