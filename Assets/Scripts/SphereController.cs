using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class SphereController : MonoBehaviour
{
    public float startRadius = 5;

    public float maxRadiusSpeed = 0.7f;

    public float maxAngleSpeed = 150;

    public float acceleration = 0.1f;

    public float stopTime = 5f;

    public float scaleDownTime = 5f;
    public float AngleSpeed
    {
        get { return angleSpeed; }
        private set { angleSpeed = value; }
    }

    public float RadiusSpeed
    {
        get { return radiusSpeed; }
        private set { radiusSpeed = value; }
    }

    public float DistanceTraveled
    {
        get { return distanceTraveled; }
        private set { distanceTraveled = value; }
    }

    public bool IsStopped
    {
        get { return isStopped; }
        private set { isStopped = value; }
    }

    public GameObject sphereModel;

    private float angleSpeed;
    private float radiusSpeed;
    private float distanceTraveled;

    private float angles;
    private float radius;

    private bool isStopped = true;

    private float currentStopTime = 0f;

    private IEnumerator sphereStopped;
    private Coroutine currentSphereStoppedRoutine;

    private IEnumerator sphereScaling;
    private Coroutine currentSphereScalingRoutine;

    private Vector3 previousLoc;
    private ParticleSystem particles;
    private Vector3 startingPos;
    private Vector3 startingScale;
    private TrailRenderer trail;
    private MeshRenderer renderer;
    void Awake()
    {
        sphereStopped = StoppedSphereRoutine();
        sphereScaling = SphereScalingRoutine();
        particles = GetComponentInChildren<ParticleSystem>();
        trail = GetComponentInChildren<TrailRenderer>();
        renderer = GetComponentInChildren<MeshRenderer>();
        startingPos = transform.position;
        startingScale = sphereModel.transform.localScale;
    }

    void Start()
    {
        Initialize();
    }

    void Update()
    {
        if (!isStopped)
        {
            MoveInSpiral();
            ChangeColour();
        }
    }

    public void StopSphere()
    {
        IsStopped = true;
        if (currentSphereStoppedRoutine != null)
        {
            StopCoroutine(currentSphereStoppedRoutine);
        }

        currentSphereStoppedRoutine = StartCoroutine(sphereStopped);
    }

    public void StartSphere()
    {
        IsStopped = false;
        if (currentSphereStoppedRoutine != null)
         {
             StopCoroutine(currentSphereStoppedRoutine);
         }

    }

    void StartScaling()
    {
        StopScaling();
        currentSphereScalingRoutine = StartCoroutine(sphereScaling);
    }

    void StopScaling()
    {
        if (currentSphereScalingRoutine != null)
        {
            StopCoroutine(currentSphereScalingRoutine);
        }
    }

    public void Initialize()
    {
        IsStopped = true;
        DistanceTraveled = 0;
        angles = 0;
        radius = startRadius;
        AngleSpeed = 0;
        RadiusSpeed = 0;
        angles = Mathf.Max(0, Mathf.PI);
        radius = Mathf.Max(0, radius);
        sphereModel.transform.localScale = startingScale;
        transform.position = startingPos;
        trail.Clear();
        if (currentSphereStoppedRoutine != null)
        {
            StopCoroutine(currentSphereStoppedRoutine);
        }
        StopScaling();
    }

    void PlayEndEffect()
    {
        particles.Play();
    }

    void ChangeColour()
    {
        renderer.material.color = new Color(transform.position.x, transform.position.z, 0.5f);
    }

    void MoveInSpiral()
    {
        if (AngleSpeed < maxAngleSpeed)
            AngleSpeed += (acceleration * Time.deltaTime * maxAngleSpeed);
        else
            AngleSpeed = maxAngleSpeed;

        if (RadiusSpeed < maxRadiusSpeed)
            RadiusSpeed += (acceleration * Time.deltaTime * maxRadiusSpeed);
        else
            RadiusSpeed = maxRadiusSpeed;

        
        angles += Time.deltaTime * AngleSpeed;
        radius -= Time.deltaTime * RadiusSpeed;

        if (radius <= 0)
        {
            transform.position = Vector3.zero;
            IsStopped = true;
            StartScaling();
        }
        else
        {
            RecordDistance();
            float x = radius * Mathf.Cos(Mathf.Deg2Rad * angles);
            float z = radius * Mathf.Sin(Mathf.Deg2Rad * angles);
            transform.position = new Vector3(x, 0, z);
        }
    }

    private IEnumerator StoppedSphereRoutine()
    {
        while (true)
        {
            if (isStopped)
            {
                AngleSpeed = 0;
                RadiusSpeed = 0;
                yield return new WaitForSeconds(stopTime);
                StartSphere();
            }

            yield return null;

        }
    }
    private IEnumerator SphereScalingRoutine()
    {
        float i = 0;
        float rate = 1 / scaleDownTime;
        while (true)
        {
            if (i < 1)
            {
                i += Time.deltaTime * rate;
                sphereModel.transform.localScale = Vector3.Lerp(startingScale, Vector3.zero, i); ;
            }
            else
            {
                i = 0;
                PlayEndEffect();
                StopScaling();
            }

            yield return null;

        }
    }
    void RecordDistance()
    {
        DistanceTraveled += Vector3.Distance(transform.position, previousLoc);
        previousLoc = transform.position;
    }

}
