using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public SphereController sphere;
    public UIManager uiManager;

    private bool gameStarted = false;
    void Update()
    {
        if (gameStarted)
        {
            if (Input.GetKeyDown(KeyCode.Space) && !sphere.IsStopped)
            {
                sphere.StopSphere();
            }

            uiManager.distancePanel.SetActive(sphere.IsStopped);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public void StartGame()
    {
        if (!gameStarted)
        {
            gameStarted = true;
            sphere.StartSphere();
            uiManager.UpdateStartingValues();
        }

    }

    public void ResetGame()
    {
        gameStarted = false;
        sphere.Initialize();
        uiManager.distancePanel.SetActive(false);
    }
}
